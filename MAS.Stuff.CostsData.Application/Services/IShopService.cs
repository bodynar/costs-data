﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Application.Services {
    public interface IShopService : ISimpleCRUDService<Shop> {
        // todo: add methods

        double GetAveragePurchasesCost(Guid shopID);
    }
}
