﻿using System;
using System.Collections.Generic;

namespace MAS.Stuff.CostsData.Application.Services {
    public interface ISimpleCRUDService<TModel> {
        void Add(TModel model);
        TModel Get(Guid ID);
        IEnumerable<TModel> GetPage(int pageNumber, int pageSize = 50);
        IEnumerable<TModel> GetAll();
        void Update(TModel model);
        void Remove(Guid ID);
        void Remove(TModel model);
    }
}
