﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Application.Services {
    public interface IPurchaseService : ISimpleCRUDService<Purchase> {
        // todo: add methods

        IEnumerable<Purchase> GetByDate(DateTime date);

        IEnumerable<Purchase> GetByShop(Guid shopID);
    }
}
