﻿using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Application.Services {
    public interface IPurchasedProductService : ISimpleCRUDService<PurchasedProduct> {
        // todo: add methods
    }
}
