﻿using System;
using System.Collections.Generic;
using System.Linq;
using MAS.Stuff.CostsData.Accessor.Services;
using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Application.Services.Impl {
    public class PurchaseService : IPurchaseService {
        private readonly IRepository<Purchase> _purchaseRepository;

        public PurchaseService(IRepository<Purchase> _purchaseRepository) {
            this._purchaseRepository = _purchaseRepository;
        }

        #region ISimpleCRUDService implementation

        public void Add(Purchase purchase)
            => _purchaseRepository.Add(purchase);

        public Purchase Get(Guid ID)
            => _purchaseRepository.Get(ID);

        public IEnumerable<Purchase> GetAll()
            => _purchaseRepository.GetAll().ToList();

        public IEnumerable<Purchase> GetPage(int pageNumber, int pageSize = 50)
            => _purchaseRepository.GetPage(pageNumber, pageSize).ToList();

        public void Remove(Guid ID)
            => _purchaseRepository.Remove(ID);

        public void Remove(Purchase purchase)
            => _purchaseRepository.Remove(purchase);

        public void Update(Purchase purchase)
            => _purchaseRepository.Update(purchase);

        #endregion

        #region IPurchaseService implamentation

        public IEnumerable<Purchase> GetByDate(DateTime date) 
            => GetAll().Where(purchase => purchase.Date == date);

        public IEnumerable<Purchase> GetByShop(Guid shopID)
            => GetAll().Where(purchase => purchase.ShopId == shopID);

        #endregion
    }
}
