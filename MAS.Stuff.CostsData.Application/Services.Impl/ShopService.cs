﻿using System;
using System.Collections.Generic;
using System.Linq;
using MAS.Stuff.CostsData.Accessor.Services;
using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Application.Services.Impl {
    public class ShopService : IShopService {
        private readonly IRepository<Shop> _shopRepository;
        private readonly IRepository<Purchase> _purchaseRepository;

        public ShopService(IRepository<Shop> _shopRepository, IRepository<Purchase> _purchaseRepository) {
            this._shopRepository = _shopRepository;
            this._purchaseRepository = _purchaseRepository;
        }

        #region ISimpleCRUDService implementation

        public void Add(Shop shop)
            => _shopRepository.Add(shop);

        public Shop Get(Guid ID)
            => _shopRepository.Get(ID);

        public IEnumerable<Shop> GetAll()
            => _shopRepository.GetAll().ToList();

        public IEnumerable<Shop> GetPage(int pageNumber, int pageSize = 50)
            => _shopRepository.GetPage(pageNumber, pageSize).ToList();

        public void Remove(Guid ID)
            => _shopRepository.Remove(ID);

        public void Remove(Shop shop) 
            => _shopRepository.Remove(shop);

        public void Update(Shop shop)
            => _shopRepository.Update(shop);

        #endregion

        #region IShopService implementation

        public double GetAveragePurchasesCost(Guid shopID) {
            IEnumerable<Purchase> purchases = _purchaseRepository.GetAll().Where(x => x.ShopId == shopID);

            IEnumerable<double> costs = purchases.Select(x => x.PurchasedProduct.Sum(p => p.CostPerOne * p.Count));

            return costs.Average();
        }

        #endregion
    }
}
