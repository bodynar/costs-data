﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS.Stuff.CostsData.Accessor.Services;
using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Application.Services.Impl {
    public class PurchasedProductService : IPurchasedProductService {
        private readonly IRepository<PurchasedProduct> _purchasedProductRepository;

        public PurchasedProductService(IRepository<PurchasedProduct> _purchasedProductRepository) {
            this._purchasedProductRepository = _purchasedProductRepository;
        }

        #region ISimpleCRUDService implementation

        public void Add(PurchasedProduct purchasedProduct)
            => _purchasedProductRepository.Add(purchasedProduct);

        public PurchasedProduct Get(Guid ID) 
            => _purchasedProductRepository.Get(ID);

        public IEnumerable<PurchasedProduct> GetAll()
            => _purchasedProductRepository.GetAll().ToList();

        public IEnumerable<PurchasedProduct> GetPage(int pageNumber, int pageSize = 50)
            => _purchasedProductRepository.GetPage(pageNumber, pageSize).ToList();

        public void Remove(Guid ID)
            => _purchasedProductRepository.Remove(ID);

        public void Remove(PurchasedProduct purchasedProduct)
            => _purchasedProductRepository.Remove(purchasedProduct);

        public void Update(PurchasedProduct purchasedProduct)
            => _purchasedProductRepository.Update(purchasedProduct);

        #endregion
    }
}
