﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS.Stuff.CostsData.Accessor.Services;
using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Application.Services.Impl {
    public class CategoryService : ICategoryService {
        private readonly IRepository<Category> _CategoryRepository;

        public CategoryService(IRepository<Category> _CategoryRepository) {
            this._CategoryRepository = _CategoryRepository;
        }

        #region ISimpleCRUDService implementation

        public void Add(Category category)
            => _CategoryRepository.Add(category);

        public Category Get(Guid ID)
            => _CategoryRepository.Get(ID);

        public IEnumerable<Category> GetAll()
            => _CategoryRepository.GetAll().ToList();

        public IEnumerable<Category> GetPage(int pageNumber, int pageSize = 50)
            => _CategoryRepository.GetPage(pageNumber, pageSize).ToList();

        public void Remove(Guid ID)
            => _CategoryRepository.Remove(ID);

        public void Remove(Category category)
            => _CategoryRepository.Remove(category);

        public void Update(Category category)
            => _CategoryRepository.Update(category);

        #endregion
    }
}
