﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS.Stuff.CostsData.Accessor.Services;
using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Application.Services.Impl {
    public class ProductService : IProductService {
        private readonly IRepository<Product> _productRepository;

        public ProductService(IRepository<Product> _productRepository) {
            this._productRepository = _productRepository;
        }

        #region ISimpleCRUDService implementation

        public void Add(Product product)
            => _productRepository.Add(product);

        public Product Get(Guid ID)
            => _productRepository.Get(ID);

        public IEnumerable<Product> GetAll()
            => _productRepository.GetAll().ToList();

        public IEnumerable<Product> GetPage(int pageNumber, int pageSize = 50)
            => _productRepository.GetPage(pageNumber, pageSize).ToList();

        public void Remove(Guid ID) 
            => _productRepository.Remove(ID);

        public void Remove(Product product)
            => _productRepository.Remove(product);

        public void Update(Product product)
            => _productRepository.Update(product);

        #endregion
    }
}
