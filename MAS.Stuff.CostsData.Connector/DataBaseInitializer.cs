﻿using System.Data.Entity;
using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Connector {
    public class DataBaseInitializer : CreateDatabaseIfNotExists<CostsDataContext> {

        public override void InitializeDatabase(CostsDataContext context) {
            context.Categorys.Add(new Category {
                Name = "Скидка"
            });

            context.Categorys.Add(new Category {
                Name = "Подарок"
            });


            base.InitializeDatabase(context);
        }
    }
}
