﻿using System.Data.Entity;
using MAS.Stuff.CostsData.Entities;

namespace MAS.Stuff.CostsData.Connector {
    public class CostsDataContext : DbContext {

        static CostsDataContext() {
            Database.SetInitializer(new DataBaseInitializer());
        }

        public CostsDataContext()
            : base("CostsDataContext") { }

        public static CostsDataContext Create() // todo: hm
            => new CostsDataContext();

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Shop> Shops { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Purchase> Purchases { get; set; }
        public virtual DbSet<PurchasedProduct> PurchasedProducts { get; set; }
        public virtual DbSet<Category> Categorys { get; set; }
    }
}
