﻿using System;
using System.Linq;
using MAS.Stuff.CostsData.Accessor.Services;
using MAS.Stuff.CostsData.Connector;

namespace MAS.Stuff.CostsData.Accessor {
    public abstract class RepositoryBase<TEntity> : IRepository<TEntity>
        where TEntity : class {
        private CostsDataContext _db;

        public virtual void Add(TEntity entity) 
            => _db.Set<TEntity>().Add(entity);

        public virtual TEntity Get(Guid ID) 
            => _db.Set<TEntity>().Find(ID);
        public virtual IQueryable<TEntity> GetAll() 
            => _db.Set<TEntity>().AsQueryable<TEntity>();
        public virtual IQueryable<TEntity> GetPage(int pageNumber, int pageSize = 50) 
            => _db.Set<TEntity>().AsQueryable().Skip(pageNumber * pageSize).Take(pageSize);

        [Obsolete("Method is obsolete. Update entity and invoke SaveChanges on database context")]
        public virtual void Update(TEntity entity) 
            => _db.Entry<TEntity>(entity).State = System.Data.Entity.EntityState.Modified;

        public virtual void Remove(Guid ID) {
            var entity = this.Get(ID);
            // todo:  sic
            if (entity != null)
                this.Remove(entity);
        }
        public virtual void Remove(TEntity entity) 
            => _db.Set<TEntity>().Remove(entity);
    }
}
