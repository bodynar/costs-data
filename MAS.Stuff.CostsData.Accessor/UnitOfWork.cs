﻿using System;
using System.Data.Entity;
using MAS.Stuff.CostsData.Accessor.Services;
using MAS.Stuff.CostsData.Connector;

namespace MAS.Stuff.CostsData.Accessor {
    public class UnitOfWork : IUnitOfWork, IDisposable { // todo: или перенести в IUnitOfWork дабы не было проблем с using ()

        #region Constructors

        public UnitOfWork(CostsDataContext db) {
            _db = db;
        }

        private UnitOfWork() { }

        #endregion

        private CostsDataContext _db;

        private DbContextTransaction currentTransaction;

        public void BeginTransaction() {
            if (currentTransaction != null)
                throw new InvalidOperationException("Previous transaction not ended");

            currentTransaction = _db.Database.BeginTransaction();
        }

        public bool Commit() {
            if (currentTransaction == null)
                throw new InvalidOperationException("Transaction not started");

            try
            {
                _db.SaveChanges();
                currentTransaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                EndTransaction();
                return false;
            }
        }

        public void EndTransaction() {
            if (currentTransaction != null)
            {
                currentTransaction.Rollback();
                Dispose();
            }
        }

        public void Rollback() {
            if (currentTransaction == null)
                throw new NullReferenceException("Transaction not statred");

            currentTransaction.Rollback();
        }

        public void Dispose() {
            if (currentTransaction != null)
            {
                currentTransaction.Dispose();
                currentTransaction = null;
            }
        }

        public static IUnitOfWork StartTransaction(CostsDataContext context) { // todo: hmmmm 
            IUnitOfWork unitOfWork = new UnitOfWork() {
                _db = context
            };
            unitOfWork.BeginTransaction();
            return unitOfWork;
        }
    }
}
