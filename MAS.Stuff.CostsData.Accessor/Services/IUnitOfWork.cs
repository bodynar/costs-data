﻿namespace MAS.Stuff.CostsData.Accessor.Services {
    public interface IUnitOfWork {
        void BeginTransaction();
        void EndTransaction();
        bool Commit();
        void Rollback();
    }
}
