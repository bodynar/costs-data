﻿using System;
using System.Linq;

namespace MAS.Stuff.CostsData.Accessor.Services {
    public interface IRepository<TEntity> {
        void Add(TEntity entity);
        TEntity Get(Guid ID);
        IQueryable<TEntity> GetPage(int pageNumber, int pageSize = 50);
        IQueryable<TEntity> GetAll();
        void Update(TEntity entity);
        void Remove(Guid ID);
        void Remove(TEntity entity);
    }
}
