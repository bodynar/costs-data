﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace MAS.Stuff.CostsData.WPF {
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application {
        [STAThread]
        static void Main() {
            var app = new App();

            app.Run();
        }

        static App() {

        }

        public App() {

        }
    }
}
